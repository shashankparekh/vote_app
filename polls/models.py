from __future__ import unicode_literals
from django.utils import timezone
from django.db import models
from django.contrib.auth.models import User


class Question(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	question_text = models.CharField(max_length=300)
	created = models.DateTimeField(default=timezone.now())


class Choice(models.Model):
	question = models.ForeignKey(Question, on_delete=models.CASCADE)
	choice_text = models.CharField(max_length=60)
	votes = models.IntegerField(default=0)


class Marking(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	question = models.ForeignKey(Question, on_delete=models.CASCADE)
	choice = models.ForeignKey(Choice, on_delete=models.CASCADE)
