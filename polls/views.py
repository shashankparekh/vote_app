from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.core.urlresolvers import reverse
from .models import Question, Choice, Marking
from django.utils import timezone


# Create your views here.

def sign_in(request):
	print request.POST
	username = request.POST['username']
	password = request.POST['password']
	user = authenticate(username=username, password=password)
	if user is not None:
		login(request, user)
		return HttpResponseRedirect(reverse('index', args=()))
	else:
		return render(request, 'polls/sign.html', {'error_message': "Wrong authentication"})


def index(request):
	print request.user.is_authenticated()
	if request.user.is_authenticated():
		question = list(set(Question.objects.all()) - set(request.user.question_set.all()))
		return render(request, 'polls/index.html', {'questions': question})
	else:
		return render(request, 'polls/sign.html', {'error_message': "Please sign in"})


def sign_out(request):
	logout(request)
	return render(request, 'polls/sign.html', {'error_message': "Successfully logout"})


def create_question(request):
	if request.method=='POST':
		if not request.POST['question_text'] or not request.POST['choice_array']:
			return HttpResponseRedirect(reverse('create_question'))

		question_text = request.POST['question_text']
		choice_options = request.POST['choice_array'].split(',')
		user = request.user
		question = user.question_set.create(question_text=question_text, created=timezone.now())
		for choice in choice_options:
			question.choice_set.create(choice_text=choice).save()
		question.save()
		return render(request, 'polls/create_question.html', {'error_message': "Question Successfully created"})
	else:
		return render(request, 'polls/create_question.html')

def get_question(request, question_id):
	if isinstance(question_id, unicode):
		question_id = decrypt_function(question_id)
	if request.user.is_authenticated():
		encrypt = encrypt_function(username=request.user.username, question_id=question_id)
		url = 'http://localhost:8000/polls/question/'+encrypt
		question = Question.objects.filter(id=question_id)[0]
		if  Marking.objects.filter(user_id=request.user.id,question_id=question.id).count()	> 0:
			data = {"message": "Question already Attempted", 'url': url}
		else:
			data = {'question': question, 'url': url}
		return render(request, 'polls/get_question.html', data)
	else:
		return render(request, 'polls/sign.html', {'error_message': "Please sign in"})

def my_question(request):
	if request.user.is_authenticated():
		question = request.user.question_set.all()
		return render(request, 'polls/questions.html', {'questions': question})
	else:
		return render(request, 'polls/sign.html', {'error_message': "Please sign in"})


def vote(request, question_id):
	question = Question.objects.filter(id=question_id)[0]
	try: 
		selected_choice = question.choice_set.get(pk=request.POST['choice'])
	except (KeyError, Choice.DoesNotExist):
		return render(request, 'polls/get_question.html'), {
			'question': question,
			'error_message': "Please select one option"
		}
	else:
		selected_choice.votes += 1
		selected_choice.save()
		Marking(user_id=request.user.id, question_id=question.id, choice_id=selected_choice.id).save()
		return  HttpResponseRedirect(reverse('answer', args=(question.id,)))

def answer(request, question_id):
	question = Question.objects.filter(id=question_id)[0]
	return render(request, 'polls/answer.html', {'question': question})	

def encrypt_function(username, question_id):
	return str(question_id)+'-'+username

def decrypt_function(value):
	return value.split('-')[0]


