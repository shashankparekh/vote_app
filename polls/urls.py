from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login/$', views.sign_in, name='sign_in'),
    url(r'^logout/$', views.sign_out, name='sign_out'),
    url(r'^question/$', views.create_question, name='create_question'),
    url(r'^questions/$', views.my_question, name='my_question'),
    url(r'^question/(?P<question_id>[0-9a-z-]+)/$', views.get_question, name='get_question'),
    url(r'^question/(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
    url(r'^question/(?P<question_id>[0-9]+)/answer/$', views.answer, name='answer')
]